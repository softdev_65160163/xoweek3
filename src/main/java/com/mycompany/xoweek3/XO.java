/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xoweek3;

/**
 *
 * @author bbnpo
 */
class XO {

    static boolean checkWin(String[][] board, String player) {
        if (checkRow(board, player)) {
            return true;
        } else if (checkCol(board, player)) {
            return true;
        } else if (checkX1(board, player)) {
            return true;
        }else if (checkX2(board, player)) {
            return true;
        }else if (checkDraw(board, player)) {
            return true;}
        
        return false;
    }

    private static boolean checkRow(String[][] board, String player) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(board, player, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] board, String player, int row) {
        return board[row][0] == (player) && board[row][1] == (player) && board[row][2] == (player);
    }

    private static boolean checkCol(String[][] board, String player) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(board, player, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] board, String player, int col) {
        return board[0][col] == (player) && board[1][col] == (player) && board[2][col] == (player);
    }
      
    

    private static boolean checkX1(String[][] board, String player) {
        return board[0][0] == (player) && board[1][1] == (player) && board[2][2] == (player);
    }
    private static boolean checkX2(String[][] board, String player) {
        return board[0][2] == (player) && board[1][1] == (player) && board[2][0] == (player);
    }

       private static boolean checkDraw(String[][] table, String player) {
        for(int i=0; i<3;i++){
            for(int j=0; j<3;j++){
                if(table[i][j].equals(player)){
                    int a = 0;
                    a++;
                    if(a == 8){
                        return true;
                    }
                }
            }
        }
        return false;
    }

}

    
   
