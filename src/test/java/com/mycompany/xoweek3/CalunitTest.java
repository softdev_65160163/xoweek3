/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.xoweek3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author bbnpo
 */
public class CalunitTest {

    public CalunitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow0_o_output_truee() {
        String[][] board = {{"O", "O", "O",}, {"-", "-", "-",}, {"-", "-", "-",}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow1_o_output_truee() {
        String[][] board = {{"-", "-", "-",}, {"O", "O", "O",}, {"-", "-", "-",}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow2_o_output_truee() {
        String[][] board = {{"-", "-", "-",}, {"-", "-", "-",}, {"O", "O", "O",}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow0_X_output_truee() {
        String[][] board = {{"X", "X", "X",}, {"-", "-", "-",}, {"-", "-", "-",}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow1_X_output_truee() {
        String[][] board = {{"-", "-", "-",}, {"X", "X", "X",}, {"-", "-", "-",}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinRow2_X_output_truee() {
        String[][] board = {{"-", "-", "-",}, {"-", "-", "-",}, {"X", "X", "X",}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol0_o_output_truee() {
        String[][] board = {{"O", "-", "-",}, {"O", "-", "-",}, {"O", "-", "-",}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol1_o_output_truee() {
        String[][] board = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_o_output_truee() {
        String[][] board = {{ "-", "-","O"}, { "-", "-","O"}, { "-", "-","O"}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinCol0_X_output_truee() {
        String[][] board = {{"X", "-", "-",}, {"X", "-", "-",}, {"X", "-", "-",}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol1_X_output_truee() {
        String[][] board = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWinCol2_X_output_truee() {
        String[][] board = {{ "-", "-","X"}, { "-", "-","X"}, { "-", "-","X"},};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinX1_X_output_truee() {
        String[][] board = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinX2_X_output_truee() {
        String[][] board = {{"-", "-", "X"}, {"-", "X", "-"}, {"X", "-", "-"}};
        String player = "X";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }
     @Test
    public void testCheckWinX1_o_output_truee() {
        String[][] board = {{"O", "-", "-"}, {"-", "O", "-"}, {"-", "-", "O"}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinX2_o_output_truee() {
        String[][] board = {{"-", "-", "O"}, {"-", "O", "-"}, {"O", "-", "-"}};
        String player = "O";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckDraw_o_output_truee() {
        String[][] board = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String player = "-";
        boolean result = XO.checkWin(board, player);
        assertEquals(true, result);
    }


}
