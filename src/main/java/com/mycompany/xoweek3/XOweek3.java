/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.xoweek3;

import java.util.Scanner;

/**
 *
 * @author bbnpo
 */
public class XOweek3 {

    static String[][] board = {{"-", "-", "-",}, {"-", "-", "-",}, {"-", "-", "-",}};
    static String player = "X";
    static int row, col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printBorad();
            printTurn();
            intputRowCol();

            if (checkWin()) {
                printBorad();
                printWin();
                Continue();

                break;

            }
            if (Draw()) {
                printBorad();
                printDraw();
                Continue();
                break;
            }

            switchplayer();

        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to OX!");
    }

    private static void printBorad() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println("");

        }

    }

    private static void printTurn() {
        System.out.println(player + " turn");
    }

    private static void intputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row,col (row [0-2] and column [0-2]): 0 2");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board[row][col] == "-") {
                board[row][col] = player;
                break;
            }

        }

    }

    private static void switchplayer() {
        if (player == "X") {
            player = "O";
        } else {
            player = "X";

        }

    }

    private static void printWin() {
        System.out.println(player + " wins");

    }

    private static boolean checkWin() {
        if (checkRow(board, player)) {
            return true;
        } else if (checkCol(board, player)) {
            return true;
        } else if (checkX1(board, player)) {
            return true;
        } else if (checkX2(board, player)) {
            return true;
        }

        return false;
    }

    private static boolean checkX1(String[][] board, String player) {
        return board[0][0] == (player) && board[1][1] == (player) && board[2][2] == (player);
    }

    private static boolean checkX2(String[][] board, String player) {
        return board[0][2] == (player) && board[1][1] == (player) && board[2][0] == (player);
    }

    private static boolean checkRow(String[][] board, String player) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(board, player, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] board, String player, int row) {
        return board[row][0] == (player) && board[row][1] == (player) && board[row][2] == (player);
    }

    private static boolean checkCol(String[][] board, String player) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(board, player, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] board, String player, int col) {
        return board[0][col] == (player) && board[1][col] == (player) && board[2][col] == (player);
    }

    private static void printDraw() {
        System.out.println("It's a draw!");
    }

    private static boolean Draw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;
    }

    private static void Continue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Continue (y/n): ");
        String playAgain = sc.next();

        if (playAgain.equalsIgnoreCase("y")) {
            resetBoard();
            main(null);
        } else {

        }

    }

    private static void resetBoard() {
        board = new String[][]{{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        player = "X";
        row = 0;
        col = 0;
    }

}
